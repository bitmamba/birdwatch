package dk.stuntfire.birdwatch;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by stuntfire on 13/03/2018.
 */

public class Bird implements Serializable {
    @SerializedName("Created")
    private String Created;

    @SerializedName("Id")
    private int Id;

    @SerializedName("NameDanish")
    private String NameDanish;

    @SerializedName("NameEnglish")
    private String NameEnglish;

    @SerializedName("PhotoUrl")
    private String PhotoUrl;

    @SerializedName("UserId")
    private String UserId;

    public Bird(){

    }

    public Bird(String created, int id, String nameDanish, String nameEnglish, String photoUrl, String userId) {
        Created = created;
        Id = id;
        NameDanish = nameDanish;
        NameEnglish = nameEnglish;
        PhotoUrl = photoUrl;
        UserId = userId;
    }

    public Bird(String created, String nameDanish) {
        Created = created;
        NameDanish = nameDanish;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getNameDanish() {
        return NameDanish;
    }

    public void setNameDanish(String nameDanish) {
        NameDanish = nameDanish;
    }

    public String getNameEnglish() {
        return NameEnglish;
    }

    public void setNameEnglish(String nameEnglish) {
        NameEnglish = nameEnglish;
    }

    public String getPhotoUrl() {
        return PhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        PhotoUrl = photoUrl;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    @Override
    public String toString() {
        return Created + '\n' +
               Id + '\n' +
               NameDanish + '\n' +
               NameEnglish + '\n' +
               PhotoUrl + '\n' +
               UserId;
    }

}
