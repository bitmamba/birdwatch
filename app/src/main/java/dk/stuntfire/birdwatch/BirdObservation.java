package dk.stuntfire.birdwatch;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by stuntfire on 20/03/2018.
 */

public class BirdObservation implements Serializable {

    @SerializedName("BirdId")
    private int BirdId;

    @SerializedName("Comment")
    private String Comment;

    @SerializedName("Created")
    private String Created;

    @SerializedName("Id")
    private int Id;

    @SerializedName("Latitude")
    private Double Latitude;

    @SerializedName("Longitude")
    private Double Longitude;

    @SerializedName("Placename")
    private String Placename;


    @SerializedName("Population")
    private int Population;

    @SerializedName("UserId")
    private String UserId;

    @SerializedName("NameDanish")
    private String NameDanish;

    @SerializedName("NameEnglish")
    private String NameEnglish;

    public BirdObservation(){}

    public BirdObservation(int birdId, String comment, String created, int id, Double latitude, Double longitude, String placename, int population, String userId, String nameDanish, String nameEnglish) {
        BirdId = birdId;
        Comment = comment;
        Created = created;
        Id = id;
        Latitude = latitude;
        Longitude = longitude;
        Placename = placename;
        Population = population;
        UserId = userId;
        NameDanish = nameDanish;
        NameEnglish = nameEnglish;
    }

    public int getBirdId() {
        return BirdId;
    }

    public void setBirdId(int birdId) {
        BirdId = birdId;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public String getPlacename() {
        return Placename;
    }

    public void setPlacename(String placename) {
        Placename = placename;
    }

    public int getPopulation() {
        return Population;
    }

    public void setPopulation(int population) {
        Population = population;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getNameDanish() {
        return NameDanish;
    }

    public void setNameDanish(String nameDanish) {
        NameDanish = nameDanish;
    }

    public String getNameEnglish() {
        return NameEnglish;
    }

    public void setNameEnglish(String nameEnglish) {
        NameEnglish = nameEnglish;
    }

    @Override
    public String toString() {
        return BirdId + '\n' +
               Comment + '\n' +
               Created + '\n' +
               Id + '\n' +
               Latitude + '\n' +
               Longitude + '\n' +
               Placename + '\n' +
               Population + '\n' +
               UserId + '\n' +
               NameDanish + '\n' +
               NameEnglish;
    }
}
