package dk.stuntfire.birdwatch;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class GetAllBirdObservationsActivity extends AppCompatActivity implements GestureDetector.OnGestureListener {

    private static final String TAG = "SHIT";
    private GestureDetector gestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bird_observations);

        DownloadTextTask textTask = new DownloadTextTask();

        textTask.execute("http://birdobservationservice.azurewebsites.net/Service1.svc/observations");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        gestureDetector = new GestureDetector(this, this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d(TAG, "onTouch: " + event);
        boolean eventHandlingFinished = true;
        //return eventHandlingFinished;
        return gestureDetector.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        boolean leftSwipe = motionEvent.getX() > motionEvent1.getX();
        if (leftSwipe) {
            Intent intent = new Intent(this, AddBirdObservationActivity.class);
            startActivity(intent);
        }
        return true; // done
    }

    private class DownloadTextTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            try {
                return downloadText(urls[0]);
            } catch (IOException ex) {
                Log.e(TAG, "DownloadTextTask: " + ex.toString());
                cancel(true);
                return ex.toString();
            }
        }

        @Override
        protected void onPostExecute(String text) {

            TextView textView = findViewById(R.id.main_textView);
            //try {

            GsonBuilder gsonBuilder = new GsonBuilder();

            Gson gson = gsonBuilder.create();

            //Gson gson = new GsonBuilder().setDateFormat(DateFormat.FULL, DateFormat.FULL).setPrettyPrinting().create();
            final BirdObservation[] birdObservations = gson.fromJson(text.toString(), BirdObservation[].class);

            //point at our layout ListView in activity_get_all_birds.xmlbirds.xml
            ListView listView = findViewById(R.id.main_bird_observations);

            //to populate the list view with data, we need an adapter
            ArrayAdapter<BirdObservation> listAdapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_list_item_1, birdObservations);

            //connect our adapter to the ListView
            listView.setAdapter(listAdapter);

            /*} catch (JSONException ex) {
                textView.setText(ex.toString());
                Log.e("BIRDS", ex.getMessage());
            }*/
        }
    }

    private String downloadText(String url) throws IOException {
        InputStream in = openHttpConnection(url);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder stringBuilder = new StringBuilder();
        while (true) {
            String line = reader.readLine();
            if (line == null) break;
            stringBuilder.append(line);
        }
        return stringBuilder.toString();
    }

    private InputStream openHttpConnection(String urlString) throws IOException {
        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();
        if (!(conn instanceof HttpURLConnection)) {
            throw new IOException("Not an HTTP connection " + conn.getURL());
        }
        HttpURLConnection httpConn = (HttpURLConnection) conn;
        httpConn.setAllowUserInteraction(false); // No user interaction like dialogs, etc.
        httpConn.setInstanceFollowRedirects(true);    // follow redirects, response code 3xxx
        httpConn.setRequestMethod("GET");
        httpConn.connect();
        int responseCode = httpConn.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            return httpConn.getInputStream();
        } else {
            throw new IOException("HTTP response " + responseCode + " " + httpConn.getResponseMessage());
        }
    }

}
