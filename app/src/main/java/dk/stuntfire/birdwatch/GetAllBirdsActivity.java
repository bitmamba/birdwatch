package dk.stuntfire.birdwatch;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

public class GetAllBirdsActivity extends AppCompatActivity {

    private static final String LOG_TAG = "SHIT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_all_birds);

        DownloadTextTask textTask = new DownloadTextTask();
        textTask.execute("http://birdobservationservice.azurewebsites.net/Service1.svc/birds");
    }

    private class DownloadTextTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            try {
                return downloadText(urls[0]);
            } catch (IOException ex) {
                Log.e(LOG_TAG, "DownloadTextTask: " + ex.toString());
                cancel(true);
                return ex.toString();
            }
        }

        @Override
        protected void onPostExecute(String text) {

            TextView textView = findViewById(R.id.main_textView);
            //try {

                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());

                Gson gson = gsonBuilder.create();

                //Gson gson = new GsonBuilder().setDateFormat(DateFormat.FULL, DateFormat.FULL).setPrettyPrinting().create();
                final Bird[] birds = gson.fromJson(text.toString(), Bird[].class);

                //point at our layout ListView in activity_get_all_birdsall_birds.xml
                ListView listView = findViewById(R.id.getAllBirds_listView);

                //to populate the list view with data, we need an adapter
                ArrayAdapter<Bird> listAdapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_list_item_1, birds);

                //connect our adapter to the ListView
                listView.setAdapter(listAdapter);

            /*} catch (JSONException ex) {
                textView.setText(ex.toString());
                Log.e("BIRDS", ex.getMessage());
            }*/
        }

        //region fetch as ArrayList, doesn't work 😡
        /*@Override
        protected void onPostExecute(String text) {

            TextView textView = findViewById(R.id.main_textView);

            final ArrayList<Bird> birds = new ArrayList<>();

            //point at our layout ListView in activity_get_all_birdsall_birds.xml
            ListView listView = findViewById(R.id.getAllBirds_listView);

            //to populate the list view with data, we need an adapter
            ArrayAdapter<Bird> listAdapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_list_item_1, birds);

            //connect our adapter to the ListView
            listView.setAdapter(listAdapter);

            try {

                JSONArray array = new JSONArray(text);

                for (int i = 0; i < array.length(); i++) {

                    JSONObject obj = array.getJSONObject(i);

                    String created = obj.getString("Created");
                    int id = obj.getInt("Id");
                    String nameDanish = obj.getString("NameDanish");
                    String nameEnglish = obj.getString("NameEnglish");
                    String photoUrl = obj.getString("PhotoUrl");
                    int userId = obj.getInt("UserId");

                    Bird bird = new Bird(created, id, nameDanish, nameEnglish, photoUrl, userId);
                    birds.add(bird);

                    //henter kun dato og navn
                    //Bird bird = new Bird(created, nameDanish);
                    //birds.add(bird);
                }
            } catch(JSONException ex){
                textView.setText(ex.toString());
            }
        }*/
        //endregion
        //region fetch all birds as one JSONObject
        /*        @Override
            protected void onPostExecute(String text) {

                TextView view = findViewById(R.id.main_textView);
                try {
                    JSONObject jsonObject = new JSONObject(text);
                    final String Created = jsonObject.getString("Created");
                    final String Id = jsonObject.getString("Id");
                    final String NameDanish = jsonObject.getString("NameDanish");
                    final String NameEnglish = jsonObject.getString("NameEnglish");
                    final String PhotoUrl = jsonObject.getString("PhotoUrl");
                    final String UserId = jsonObject.getString("UserId");
                    view.setText(Created + ": \n" +
                            Id + ": \n" +
                            NameDanish + ": \n" +
                            NameEnglish + ": \n" +
                            PhotoUrl + ": \n" +
                            UserId + ": \n");
                } catch (JSONException ex) {
                    view.setText(ex.toString());
             }*/
        //endregion
    }


    private String downloadText(String url) throws IOException {
        InputStream in = openHttpConnection(url);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder stringBuilder = new StringBuilder();
        while (true) {
            String line = reader.readLine();
            if (line == null) break;
            stringBuilder.append(line);
        }
        return stringBuilder.toString();
    }

    private InputStream openHttpConnection(String urlString) throws IOException {
        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();
        if (!(conn instanceof HttpURLConnection)) {
            throw new IOException("Not an HTTP connection " + conn.getURL());
        }
        HttpURLConnection httpConn = (HttpURLConnection) conn;
        httpConn.setAllowUserInteraction(false); // No user interaction like dialogs, etc.
        httpConn.setInstanceFollowRedirects(true);    // follow redirects, response code 3xxx
        httpConn.setRequestMethod("GET");
        httpConn.connect();
        int responseCode = httpConn.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            return httpConn.getInputStream();
        } else {
            throw new IOException("HTTP response " + responseCode + " " + httpConn.getResponseMessage());
        }
    }
}

